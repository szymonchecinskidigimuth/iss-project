namespace RpiClient.Enums
{
    public enum LinguisticValue
    {
        NegativeLarge = 0,
        NegativeMedium,
        NegativeSmall,
        Zero,
        PositiveSmall,
        PositiveMedium,
        PositiveLarge
    }
}