using System;
using Common.Models;

namespace RpiClient.Models
{
    public class Reading
    {
        public long Id { get; set; }
        
        public int FanSpeed { get; set; }
        
        public double Temperature { get; set; }
        
        public DateTime MeasurementTime { get; set; }

        public ReadingsDto ToDto()
        {
            return new ReadingsDto
            {
                FanSpeed = FanSpeed,
                Temperature = Temperature,
                MeasurementTime = MeasurementTime.ToString("yyyy-MM-ddThh:mm:ss.ffff") + "00"
            };
        }
    }
}