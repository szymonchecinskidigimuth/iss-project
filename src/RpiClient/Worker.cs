using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Common;
using Common.Models;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RpiClient.Models;
using RpiClient.Services;

namespace RpiClient
{
    public class Worker : BackgroundService
    {
        private readonly SemaphoreSlim _semaphore = new SemaphoreSlim(1, 1);

        private readonly ILogger<Worker> _logger;
        private readonly IMotorService _motorService;
        private readonly ICpuTemperatureService _cpuTemperatureService;
        private readonly HubConnection _connection;
        private readonly IRegulator _regulator;
        private readonly RpiDbContext _dbContext;
        
        public Worker(
            ILogger<Worker> logger,
            IMotorService motorService,
            IRetryPolicy retryPolicy,
            ICpuTemperatureService cpuTemperatureService,
            IRegulator regulator,
            RpiDbContext dbContext)
        {
            _logger = logger;
            _motorService = motorService;
            _cpuTemperatureService = cpuTemperatureService;
            _regulator = regulator;
            _dbContext = dbContext;
            _connection = new HubConnectionBuilder()
                .WithUrl("http://10.29.25.175:5000/iot")
                .WithAutomaticReconnect(retryPolicy)
                .Build();

            _connection.On<StartExperimentDto>(HubActions.ReceiveStartExperiment, StartExperiment);
            _connection.On<GetHistoricalDataDto>(HubActions.RpiBeginGetHistoricalData, GetHistoricalData);
        }

        private async Task GetHistoricalData(GetHistoricalDataDto dto)
        {
            IEnumerable<ReadingsDto> historicalReadings;
            
            await _semaphore.WaitAsync();
            
            try
            {
                historicalReadings = await _dbContext.GetHistoricalReadings(dto);
            }
            finally
            {
                _semaphore.Release();
            }

            await _connection.SendAsync(HubActions.RpiEndGetHistoricalData, historicalReadings);
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                var fanSpeed = _motorService.CurrentSpeed;
                var temperature = _cpuTemperatureService.Temperature;

                if (temperature > 0)
                {
                    var data = new Reading
                    {
                        FanSpeed = fanSpeed,
                        Temperature = temperature,
                        MeasurementTime = DateTime.Now
                    };
                    
                    await _semaphore.WaitAsync(cancellationToken);
                
                    try
                    {
                        await _dbContext.Readings.AddAsync(data, cancellationToken);
                        var signalrTask = _connection.SendAsync(HubActions.SendCurrentData, data.ToDto(), cancellationToken);
                        var dbTask = _dbContext.SaveChangesAsync(cancellationToken);

                        await Task.WhenAll(signalrTask, dbTask);
                    }
                    finally
                    {
                        _semaphore.Release();
                    }
                }
                
                await Task.Delay(1000, cancellationToken);
            }

            await base.StopAsync(cancellationToken);
        }

        public override async Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Connecting to SignalR hub");
            await _connection.StartAsync(cancellationToken);
            _logger.LogInformation("Connected");

            await _dbContext.Database.MigrateAsync(cancellationToken);
            
            _cpuTemperatureService.Start(cancellationToken);

            await base.StartAsync(cancellationToken);
        }

        private async Task StartExperiment(StartExperimentDto dto)
        {
            _logger.LogInformation("Received StartExperiment command");

            _motorService.Start();

            await _regulator.Regulate(dto);
            
            _motorService.Stop();

            _logger.LogInformation("Experiment finished");
        }
    }
}