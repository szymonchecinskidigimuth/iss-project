using System;
using Microsoft.AspNetCore.SignalR.Client;

namespace RpiClient.RetryPolicies
{
    public class RetryPolicy : IRetryPolicy
    {
        public TimeSpan? NextRetryDelay(RetryContext retryContext)
        {
            return TimeSpan.FromSeconds(2);
        }
    }
}