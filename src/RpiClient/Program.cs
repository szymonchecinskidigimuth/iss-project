using System.Net;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RpiClient.RetryPolicies;
using RpiClient.Services;

namespace RpiClient
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices(ConfigureServices);

        private static void ConfigureServices(HostBuilderContext hostContext, IServiceCollection services)
        {
            services
                .AddSingleton<IMotorService, MotorService>()
                .AddSingleton<IRetryPolicy, RetryPolicy>()
                .AddTransient<IRegulator, PidRegulator>()
                //.AddTransient<IRegulator, FuzzyRegulator>()
                .AddSingleton<ICpuTemperatureService, CpuTemperatureService>()
                .AddDbContext<RpiDbContext>();
            services.AddHostedService<Worker>();
        }
    }
}