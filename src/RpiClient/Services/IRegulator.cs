using System.Threading.Tasks;
using Common.Models;

namespace RpiClient.Services
{
    public interface IRegulator
    {
        Task Regulate(StartExperimentDto dto);
    }
}