using System.Threading;

namespace RpiClient.Services
{
    public interface ICpuTemperatureService
    {
        void Start(CancellationToken cancellationToken);
        
        double Temperature { get; }
    }
}