using System;
using System.Device.Gpio;
using System.Device.Pwm;

namespace RpiClient.Services
{
    public class MotorService : IMotorService
    {
        private readonly GpioController _gpioController;
        private readonly PwmChannel _pwmChannel;
        private const int DirectionPin = 16;

        public MotorService()
        {
            _gpioController = new GpioController();
            _pwmChannel = PwmChannel.Create(0, 0, 50, 0);
        }
        
        public int CurrentSpeed { get; private set; }

        public void Start()
        {
            _gpioController.OpenPin(DirectionPin, PinMode.Output);
            _gpioController.Write(DirectionPin, PinValue.High);
            _pwmChannel.Start();
        }

        public void SetMotorSpeed(int speed)
        {
            CurrentSpeed = speed;
            _pwmChannel.DutyCycle = speed / 100.0;
        }

        public void Stop()
        {
            CurrentSpeed = 0;
            _gpioController.ClosePin(DirectionPin);
            _pwmChannel.Stop();
        }

        public void Dispose()
        {
            _gpioController?.Dispose();
            _pwmChannel?.Dispose();
        }
    }
}