using System;

namespace RpiClient.Services
{
    public interface IMotorService : IDisposable
    {
        void Start();
        void SetMotorSpeed(int speed);
        void Stop();
        int CurrentSpeed { get; }
    }
}