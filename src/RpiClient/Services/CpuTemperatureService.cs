using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RpiClient.Services
{
    public class CpuTemperatureService : ICpuTemperatureService
    {
        public double Temperature { get; private set; } 

        public void Start(CancellationToken cancellationToken)
        {
            Task.Run(async () =>
            {
                var list = new List<double>();
                var startInfo = new ProcessStartInfo
                {
                    FileName = "/bin/bash",
                    Arguments = $"-c \"vcgencmd measure_temp\"",
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                };
                
                while (!cancellationToken.IsCancellationRequested)
                {
                    for (var i = 0; i < 20; i++)
                    {
                        using var process = Process.Start(startInfo);
                        using var streamReader = process.StandardOutput;
                        var result = await streamReader.ReadToEndAsync();
                        list.Add(double.Parse(result.Substring(5, 4)));
                    }
                    list.Remove(list.Min());
                    list.Remove(list.Max());
                    Temperature = Math.Round(list.Average(), 1);
                    list.Clear();
                }
            }, cancellationToken);
        }
    }
}