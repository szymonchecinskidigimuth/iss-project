using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Common.Models;

namespace RpiClient.Services
{
    public class PidRegulator : IRegulator
    {
        private const double Kp = 15;
        private const double Td = 6;
        private const double Ti = 4000;

        private readonly ICpuTemperatureService _cpuTemperatureService;
        private readonly IMotorService _motorService;
        private double _differenceSum;
        private double _lastDifference;

        public PidRegulator(ICpuTemperatureService cpuTemperatureService, IMotorService motorService)
        {
            _cpuTemperatureService = cpuTemperatureService;
            _motorService = motorService;
        }

        public async Task Regulate(StartExperimentDto dto)
        {
            _differenceSum = 0;
            _lastDifference = 0;
            var targetTemperature = dto.TargetTemperature;

            var stopwatch = Stopwatch.StartNew();
            var lastTime = 1;

            while (stopwatch.Elapsed < TimeSpan.FromMinutes(60))
            {
                var newSpeed = GetNewSpeed(targetTemperature, (stopwatch.ElapsedMilliseconds - lastTime) / 1000.0);

                newSpeed = Math.Max(0, Math.Min(100, newSpeed));
                
                _motorService.SetMotorSpeed(newSpeed);

                await Task.Delay(1100);
            }
        }

        private int GetNewSpeed(int targetTemperature, double deltaTime)
        {
            var difference = _cpuTemperatureService.Temperature - targetTemperature;
            _differenceSum += difference;
            var deltaDifference = difference - _lastDifference;
            var kPart = Kp * difference;
            var tiPart = Kp * deltaTime * _differenceSum / Ti;
            var tdPart = Kp * Td * deltaDifference / deltaTime;
            _lastDifference = difference;

            Console.WriteLine($"K: {(int)kPart}, Ti: {(int)tiPart}, Td: {(int)tdPart}");
            Console.WriteLine((int)(kPart + tiPart + tdPart));

            return (int)(kPart + tiPart + tdPart);
        }
    }
}