using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Common.Models;
using RpiClient.Enums;

namespace RpiClient.Services
{
    public class FuzzyRegulator : IRegulator
    {
        private readonly IDictionary<LinguisticValue, (double, double)> _temperatureDifferenceMembership =
            new Dictionary<LinguisticValue, (double, double)>()
            {
                [LinguisticValue.NegativeLarge] = (-8, -4), // left side open
                [LinguisticValue.NegativeMedium] = (-6, -2),
                [LinguisticValue.NegativeSmall] = (-4, 0),
                [LinguisticValue.Zero] = (-2, 2),
                [LinguisticValue.PositiveSmall] = (0, 4),
                [LinguisticValue.PositiveMedium] = (2, 6),
                [LinguisticValue.PositiveLarge] = (4, 8) // right side open
            };
        
        private readonly IDictionary<LinguisticValue, (double, double)> _temperatureChangeMembership =
            new Dictionary<LinguisticValue, (double, double)>()
            {
                [LinguisticValue.NegativeLarge] = (-0.8, -0.4), // left side open
                [LinguisticValue.NegativeMedium] = (-0.6, -0.2),
                [LinguisticValue.NegativeSmall] = (-0.4, 0),
                [LinguisticValue.Zero] = (-0.2, 0.2),
                [LinguisticValue.PositiveSmall] = (0, 0.4),
                [LinguisticValue.PositiveMedium] = (0.2, 0.6),
                [LinguisticValue.PositiveLarge] = (0.4, 0.8) // right side open
            };

        private readonly IDictionary<LinguisticValue, (double, double)> _errorSumMembership =
            new Dictionary<LinguisticValue, (double, double)>()
            {
                [LinguisticValue.NegativeLarge] = (-48, -24), // left side open
                [LinguisticValue.NegativeMedium] = (-36, -12),
                [LinguisticValue.NegativeSmall] = (-24, 0),
                [LinguisticValue.Zero] = (-12, 12),
                [LinguisticValue.PositiveSmall] = (0, 24),
                [LinguisticValue.PositiveMedium] = (12, 36),
                [LinguisticValue.PositiveLarge] = (24, 48) // right side open
            };

        private readonly IDictionary<LinguisticValue, (double, double)> _outputMotorMembership =
            new Dictionary<LinguisticValue, (double, double)>()
            {
                [LinguisticValue.NegativeLarge] = (0, 0),
                [LinguisticValue.NegativeMedium] = (1, 15),
                [LinguisticValue.NegativeSmall] = (10, 30),
                [LinguisticValue.Zero] = (22, 32),
                [LinguisticValue.PositiveSmall] = (49.5, 82.5),
                [LinguisticValue.PositiveMedium] = (66, 99),
                [LinguisticValue.PositiveLarge] = (100, 100)
            };

        private readonly IDictionary<(LinguisticValue, LinguisticValue, LinguisticValue), LinguisticValue> _outputRules;

        private readonly IMotorService _motorService;
        private readonly ICpuTemperatureService _cpuTemperatureService;

        private double _differenceSum;
        private double _lastDifference;

        private LimitedQueue<double> _lastResults = new LimitedQueue<double>(6);

        public FuzzyRegulator(IMotorService motorService, ICpuTemperatureService cpuTemperatureService)
        {
            _motorService = motorService;
            _cpuTemperatureService = cpuTemperatureService;

            _outputRules = GetOutputRules();
        }

        public async Task Regulate(StartExperimentDto dto)
        {
            Console.WriteLine("Start to regulate with fuzzy");
            _differenceSum = 0;
            _lastDifference = 0;
            _lastResults = new LimitedQueue<double>(6);
            var targetTemperature = dto.TargetTemperature;

            var stopwatch = Stopwatch.StartNew();
            var lastTime = 1;

            while (stopwatch.Elapsed < TimeSpan.FromMinutes(60))
            {
                var newSpeed = GetNewSpeed(targetTemperature, (stopwatch.ElapsedMilliseconds - lastTime) / 1000.0);

                newSpeed = Math.Max(0, Math.Min(100, newSpeed));

                _motorService.SetMotorSpeed(newSpeed);

                await Task.Delay(1000);
            }
        }

        private int GetNewSpeed(int targetTemperature, double deltaTime)
        {
            var difference = _cpuTemperatureService.Temperature - targetTemperature;
            _differenceSum += difference;
            _lastResults.Enqueue(difference);
            var deltaDifference = (_lastResults.Take(3).Sum() - _lastResults.TakeLast(3).Sum()) / 3.0;
            Console.WriteLine(deltaDifference);
            _lastDifference = difference;
            
            var temperatureMembershipTable = GetMembershipTableForTemperature(difference);
            var changeMembershipTable = GetMembershipTableForTemperatureChange(deltaDifference);
            var sumMembershipTable = GetMembershipTableForErrorSum(_differenceSum);

            var fuzzyResult = GetEmptyResultDictionary();
            
            foreach (var ruleSet in _outputRules)
            {
                var temperatureValue = temperatureMembershipTable[ruleSet.Key.Item1];
                var changeValue = changeMembershipTable[ruleSet.Key.Item2];
                var sumValue = sumMembershipTable[ruleSet.Key.Item3];

                fuzzyResult[ruleSet.Value] += new [] { temperatureValue, changeValue, sumValue }.Min();
            }
            
            return (int)DefuzzifyOutput(fuzzyResult, _outputMotorMembership);
        }

        private IDictionary<LinguisticValue, double> GetMembershipTableForErrorSum(double errorSum)
        {
            return GetMembershipOfValueForRules(_errorSumMembership, errorSum);
        }
        
        private IDictionary<LinguisticValue, double> GetMembershipTableForTemperatureChange(double temperatureDelta)
        {
            return GetMembershipOfValueForRules(_temperatureChangeMembership, temperatureDelta);
        }
        
        private IDictionary<LinguisticValue, double> GetMembershipTableForTemperature(double difference)
        {
            return GetMembershipOfValueForRules(_temperatureDifferenceMembership, difference);
        }

        private IDictionary<LinguisticValue, double> GetMembershipOfValueForRules(
            IDictionary<LinguisticValue, (double, double)> rules, double value)
        {
            return rules
                .Select(r =>
                    KeyValuePair.Create(r.Key, GetMembershipValue(r.Value.Item1, r.Value.Item2, value, r.Key)))
                .ToDictionary(x => x.Key, x => x.Value);
        }

        private double GetMembershipValue(double a, double c, double x, LinguisticValue value)
        {
            if (value == LinguisticValue.NegativeLarge && x < a
                || value == LinguisticValue.PositiveLarge && x > c)
            {
                return 1;
            }
            
            var b = (a + c) / 2.0;

            var first = (x - a) / (b - a);
            var second = (c - x) / (c - b);

            return Math.Max(0, Math.Min(first, second));
        }
        
        private double DefuzzifyOutput(IDictionary<LinguisticValue, double> fuzzyOutput,
            IDictionary<LinguisticValue, (double, double)> outputMembership)
        {
            var weightSum = fuzzyOutput.Sum(o => o.Value);
            var weightedRules = fuzzyOutput.Sum(o =>
            {
                var (item1, item2) = outputMembership[o.Key];
                var ruleMean = (item1 + item2) / 2.0;

                return ruleMean * o.Value;
            });

            var output = weightedRules / weightSum;

            Console.WriteLine($"Defuzzified output: {weightedRules}/{weightSum}={output}");

            return output;
        }

        private IDictionary<(LinguisticValue, LinguisticValue, LinguisticValue), LinguisticValue> GetOutputRules()
        {
            var result = new Dictionary<(LinguisticValue, LinguisticValue, LinguisticValue), LinguisticValue>();
            
            for (var temp = LinguisticValue.NegativeLarge; temp <= LinguisticValue.PositiveLarge; ++temp)
            {
                for (var deltaTemp = LinguisticValue.NegativeLarge; deltaTemp <= LinguisticValue.PositiveLarge; ++deltaTemp)
                {
                    for (var tempSum = LinguisticValue.NegativeLarge; tempSum <= LinguisticValue.PositiveLarge; ++tempSum)
                    {
                        var avg = ((int) temp + (int) deltaTemp + (int) tempSum) / 3;

                        var output = Math.Max((int)LinguisticValue.NegativeLarge, Math.Min(avg, (int)LinguisticValue.PositiveLarge));
                        
                        if (temp <= LinguisticValue.NegativeMedium)
                        {
                            output = (int)LinguisticValue.NegativeLarge;
                        }

                        result.Add((temp, deltaTemp, tempSum), (LinguisticValue)output);
                    }
                }
            }

            return result;
        }

        private IDictionary<LinguisticValue, double> GetEmptyResultDictionary()
        {
            return new Dictionary<LinguisticValue, double>
            {
                [LinguisticValue.NegativeLarge] = 0,
                [LinguisticValue.NegativeMedium] = 0,
                [LinguisticValue.NegativeSmall] = 0,
                [LinguisticValue.Zero] = 0,
                [LinguisticValue.PositiveSmall] = 0,
                [LinguisticValue.PositiveMedium] = 0,
                [LinguisticValue.PositiveLarge] = 0
            };
        }
    }
}