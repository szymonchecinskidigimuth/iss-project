﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RpiClient.Migrations
{
    public partial class Index : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Readings_MeasurementTime",
                table: "Readings",
                column: "MeasurementTime");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Readings_MeasurementTime",
                table: "Readings");
        }
    }
}
