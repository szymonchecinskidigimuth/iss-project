using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.Models;
using Microsoft.EntityFrameworkCore;
using RpiClient.Models;

namespace RpiClient
{
    public class RpiDbContext : DbContext
    {
        public DbSet<Reading> Readings { get; set; }

        public RpiDbContext(DbContextOptions<RpiDbContext> options)
            : base(options)
        {
            
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlite("Data Source=../Readings1.db");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Reading>()
                .HasIndex(r => r.MeasurementTime);
            
            base.OnModelCreating(modelBuilder);
        }

        public async Task<IEnumerable<ReadingsDto>> GetHistoricalReadings(GetHistoricalDataDto dto)
        {
            return await Readings
                .Where(r => r.MeasurementTime >= dto.From && r.MeasurementTime <= dto.To)
                .Select(r => r.ToDto()).ToListAsync();
        }
    }
}