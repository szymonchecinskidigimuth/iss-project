using System;

namespace Common.Models
{
    public class GetHistoricalDataDto
    {
        public DateTime From { get; set; }
        
        public DateTime To { get; set; }
    }
}