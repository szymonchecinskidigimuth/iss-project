using System;

namespace Common.Models
{
    public class ReadingsDto
    {
        public int FanSpeed { get; set; }
        
        public double Temperature { get; set; }
        
        public string MeasurementTime { get; set; }
    }
}