namespace Common.Models
{
    public class StartExperimentDto
    {
        public int TargetTemperature { get; set; }
    }
}