namespace Common
{
    public static class HubActions
    {
        public const string ReceiveStartExperiment = "ReceiveStartExperiment";

        public const string SendCurrentData = "SendCurrentData";

        public const string ReceiveCurrentData = "ReceiveCurrentData";

        public const string RpiBeginGetHistoricalData = "RpiBeginGetHistoricalData";

        public const string RpiEndGetHistoricalData = "RpiEndGetHistoricalData";

        public const string JsBeginGetHistoricalData = "JsBeginGetHistoricalData";

        public const string JsEndGetHistoricalData = "JsEndGetHistoricalData";
    }
}