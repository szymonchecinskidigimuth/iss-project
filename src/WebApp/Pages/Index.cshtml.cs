﻿using Common;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using WebApp.Hubs;

namespace WebApp.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly IHubContext<IotHub> _hubContext;

        public IndexModel(ILogger<IndexModel> logger, IHubContext<IotHub> hubContext)
        {
            _logger = logger;
            _hubContext = hubContext;
        }

        public void OnGet()
        {
        }
    }
}
