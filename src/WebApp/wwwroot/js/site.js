﻿const connection = new signalR.HubConnectionBuilder()
    .withAutomaticReconnect(c => 2000)
    .withUrl("/iot")
    .build();
var fanSpeedChart, tempChart, fanSpeedHistoricalChart, tempHistoricalChart;

connection.on("ReceiveCurrentData", (dto) => {
    document.getElementById('fan-speed-reading').innerText = dto.fanSpeed;
    fanSpeedChart.data.datasets[0].data.push({ x: dto.measurementTime, y: dto.fanSpeed });
    document.getElementById('temperature-reading').innerText = dto.temperature;
    tempChart.data.datasets[0].data.push({ x: dto.measurementTime, y: dto.temperature });
    fanSpeedChart.update();
    tempChart.update();
});

connection.on("JsEndGetHistoricalData", (dtos) => {
    fanSpeedHistoricalChart.data.datasets[0].data = dtos.map(dto => { return { x: dto.measurementTime, y: dto.fanSpeed }});
    tempHistoricalChart.data.datasets[0].data = dtos.map(dto => { return { x: dto.measurementTime, y: dto.temperature }});
    fanSpeedHistoricalChart.update();
    tempHistoricalChart.update();
});

async function connect() {
    await connection.start();
}

async function start() {
    const targetTemperature = Number(document.getElementById("target-temp").value);
    await connection.invoke("StartExperiment", { targetTemperature });
}

async function fetchHistoricalData() {
    const from = document.getElementById("from-date").value;
    const to = document.getElementById("to-date").value;
    await connection.invoke("JsBeginGetHistoricalData", { from, to});
}

function prepareTempChart() {
    tempChart = prepareChart('temp-chart', 'red', 'Temperature', 'Time', 'Celsius');
}

function prepareFanChart() {
    fanSpeedChart = prepareChart('result-chart', 'blue', 'Fan speed', 'Time', '%');
}

function prepareTempHistoricalChart() {
    tempHistoricalChart = prepareChart('temp-historical-chart', 'red', 'Temperature', 'Time', 'Celsius');
}

function prepareFanHistoricalChart() {
    fanSpeedHistoricalChart = prepareChart('result-historical-chart', 'blue', 'Fan speed', 'Time', '%');
}

function prepareChart(canvasId, color, name, xAxis, yAxis) {
    const chartData = {
        "type": "line",
        "data": {
            "datasets": [{
                "label": name,
                "borderColor": color,
                "backgroundColor": color,
                "fill": false,
                "data": [],
                "yAxisID": "yAxeFan"
            }]
        },
        "options": {
            "responsive": true,
            "hoverMode": "index",
            "stacked": false,
            "title": null,
            "scales": {
                "xAxes": [{
                    "type": "time",
                    "display": true,
                    "position": "bottom",
                    "id": "xAxeTime",
                    "scaleLabel": {
                        "display": true,
                        "labelString": xAxis,
                        "fontColor": "black"
                    },
                    "time": {
                        "unit": "minute",
                        "tooltipFormat": "ll"
                    }
                }],
                "yAxes": [{
                    "type": "linear",
                    "display": true,
                    "position": "left",
                    "id": "yAxeFan",
                    "scaleLabel": {
                        "display": true,
                        "labelString": yAxis,
                        "fontColor": "black"
                    }
                }]
            }
        }
    }

    const ctx = document.getElementById(canvasId);
    return new Chart(ctx, {
        type: chartData.type,
        data: chartData.data,
        options: chartData.options,
    });
}

function prepareCharts() {
    prepareFanChart();
    prepareTempChart();
    prepareFanHistoricalChart();
    prepareTempHistoricalChart();
}

window.onload = () => prepareCharts();

connect();