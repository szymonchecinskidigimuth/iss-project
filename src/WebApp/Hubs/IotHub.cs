using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Common;
using Common.Models;
using Microsoft.AspNetCore.SignalR;

namespace WebApp.Hubs
{
    public class IotHub : Hub
    {
        public Task StartExperiment(StartExperimentDto dto)
        {
            return Clients.All.SendAsync(HubActions.ReceiveStartExperiment, dto);
        }

        public Task SendCurrentData(ReadingsDto dto)
        {
            return Clients.All.SendAsync(HubActions.ReceiveCurrentData, dto);
        }

        public Task RpiEndGetHistoricalData(IEnumerable<ReadingsDto> dto)
        {
            return Clients.All.SendAsync(HubActions.JsEndGetHistoricalData, dto);
        }

        public Task JsBeginGetHistoricalData(GetHistoricalDataDto dto)
        {
            return Clients.All.SendAsync(HubActions.RpiBeginGetHistoricalData, dto);
        }
    }
}